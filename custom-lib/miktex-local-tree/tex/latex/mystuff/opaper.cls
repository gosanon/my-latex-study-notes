\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{opaper}[2022/09/20 "ox-ui + article"-based minimalistic class]

\LoadClass[fleqn,12pt]{article}

% Formatting
\pagenumbering{gobble}
\RequirePackage[
    left=1cm,
    right=1cm,
    top=1cm,
    bottom=1cm,
    bindingoffset=0cm,
    nohead,
    nofoot]{geometry}
\RequirePackage{parskip}
\RequirePackage{oxui}
\RequirePackage{setspace}
    \setstretch{1.3}
% ---


% Math
\RequirePackage{mathtools}
\RequirePackage{amsmath}
\RequirePackage[makeroom]{cancel}
\RequirePackage{latexsym}
% ---


% patch gauss macros for doing their work in `align'
% and other amsmath environments; see
% http://tex.stackexchange.com/questions/146532/
\RequirePackage{etoolbox}
\makeatletter
\patchcmd\g@matrix
 {\vbox\bgroup}
 {\vbox\bgroup\normalbaselines}% restore the standard baselineskip
 {}{}
\makeatother

\newcommand{\BAR}{%
  \hspace{-\arraycolsep}%
  \strut\vrule % the `\vrule` is as high and deep as a strut
  \hspace{-\arraycolsep}%
}
