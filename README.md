# My LaTeX study notes



## Dependencies

- MikTex (XeTeX) (+ `/custom-lib/miktex-local-tree` must be listed in "root directories")
- JetBrains Mono Font
- All other dependencies are included in `/custom-lib/miktex-local-tree/tex/latex/mystuff`
- For vscode: LaTeX Workshop extension (a bit reconfigured for XeTeX compilation by default)