from math import sin, cos, sqrt

def cot(x):
  return cos(x) / sin(x)

def f(x):
  return cot(x + 0.4) - x**2

def df(x):
  return -1/sin(x + 0.4)**2 - 2*x

# Исходные данные: 
eps = 0.5 * 10 ** (-5)
x_0 = 1
m = -3.0297
M = -0.1864

# Инициализация переменных:
x_current = x_0
x_next = x_current - f(x_current) / df(x_current)
n = 1

while (M/(2*m)) * (x_next - x_current)**2 > eps:
  print(f'n={n}\tx={x_next}\t|x - x_prev|={abs(x_next - x_current)}')
  n += 1
  x_current = x_next
  x_next = x_current - f(x_current) / df(x_current)
else:
  print(f'n={n}\tx={x_next}\t|x - x_prev|={abs(x_next - x_current)}')
