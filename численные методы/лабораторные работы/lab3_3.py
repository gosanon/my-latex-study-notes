import numpy as np

def print_matrix(M):
    for row in M:
        print(' '.join([f"{elem:.3f}" for elem in row]))
    print()


def jacobi_decomposition(A):
    # Проверка, является ли матрица квадратной
    assert len(A) == len(A[0]), "Матрица должна быть квадратной"

    # Размер матрицы
    n = len(A)

    # Инициализация матриц L, D и R
    L = [[0]*n for _ in range(n)]
    D = [[0]*n for _ in range(n)]
    R = [[0]*n for _ in range(n)]

    for i in range(n):
        for j in range(n):
            if i > j:
                L[i][j] = A[i][j]
            elif i < j:
                R[i][j] = A[i][j]
            else:
                D[i][j] = A[i][j]

    return L, D, R

# Пример использования
A = [
  [1.76, 0.06, 0.2],
  [-0.1, 0.78, 0.44],
  [-0.08, -0.42, -0.5]
]

L, D, R = jacobi_decomposition(A)

print("L:")
print_matrix(L)
print("D:")
print_matrix(D)
print("R:")
print_matrix(R)

def is_jacobi_convergent(L, D, R):
    # Собираем матрицу A
    A = np.array(L) + np.array(D) + np.array(R)

    # Вычисляем собственные значения матрицы A
    eigenvalues = np.linalg.eigvals(A)

    # Проверяем, что все собственные значения по модулю меньше 1
    print(eigenvalues)
    return all(abs(lam) < 1 for lam in eigenvalues)
  
print('Сходится ли метод:', is_jacobi_convergent(L, D, R))