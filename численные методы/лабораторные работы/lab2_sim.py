from math import sin, cos, sqrt


def cot(x):
  return cos(x) / sin(x)


def simple_iteration_method_next_x(current_x):
  x = current_x
  return sqrt(cot(x + 0.4))


# Исходные данные: 
eps = 0.5 * 10 ** (-5)
x_0 = 0.7

# Инициализация переменных:
x_current = x_0
x_next = simple_iteration_method_next_x(x_current)
n = 1

while abs(x_next - x_current) > eps:
  print(f'n={n}\tx={x_next}\t|x - x_prev|={abs(x_next - x_current)}')
  n += 1
  x_current = x_next
  x_next = simple_iteration_method_next_x(x_current)
else:
  print(f'n={n}\tx={x_next}\t|x - x_prev|={abs(x_next - x_current)}')