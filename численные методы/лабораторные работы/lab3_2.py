def print_matrix(M):
    for row in M:
        print(' '.join([f"{elem:.3f}" for elem in row]))
    print()

def swap_rows(A, row1, row2):
    A[row1], A[row2] = A[row2], A[row1]

def swap_columns(A, col1, col2):
    for i in range(len(A)):
        A[i][col1], A[i][col2] = A[i][col2], A[i][col1]

def find_pivot(A, start_row, start_col):
    max_val = abs(A[start_row][start_col])
    pivot_row, pivot_col = start_row, start_col
    for i in range(start_row, len(A)):
        for j in range(start_col, len(A[i])):
            if abs(A[i][j]) > max_val:
                max_val = abs(A[i][j])
                pivot_row, pivot_col = i, j
    return pivot_row, pivot_col

def gauss_elimination(A, b):
    n = len(A)
    for i in range(n):
        pivot_row, pivot_col = find_pivot(A, i, i)
        swap_rows(A, i, pivot_row)
        swap_rows(b, i, pivot_row)
        swap_columns(A, i, pivot_col)
        print(f"Шаг {i+1}:\nA:")
        print_matrix(A)
        print("b:", b)
        for j in range(i+1, n):
            factor = A[j][i] / A[i][i]
            for k in range(i, n):
                A[j][k] -= factor * A[i][k]
            b[j] -= factor * b[i]

def back_substitution(A, b):
    n = len(A)
    x = [0]*n
    for i in range(n-1, -1, -1):
        x[i] = (b[i] - sum(A[i][j]*x[j] for j in range(i+1, n))) / A[i][i]
        print('x', [float(f"{elem:.3f}") for elem in x])
    return x

# Пример использования:
A = [
  [ 1.76,  0.06,  0.20],
  [-0.10,  0.78,  0.44],
  [-0.08, -0.42, -0.50]
]

b = [2.36, 2.78, -2.42]
gauss_elimination(A, b)
x = back_substitution(A, b)
print(f"Решение системы: {[f'{i:.3f}' for i in x]}")
