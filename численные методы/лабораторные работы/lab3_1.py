def print_matrix(M):
    for row in M:
        print(' '.join([f"{elem:.3f}" for elem in row]))
    print()

def lu_decomposition(A):
    n = len(A)
    L = [[0]*n for _ in range(n)]
    U = [[0]*n for _ in range(n)]

    for i in range(n):
        L[i][i] = 1
        for j in range(i+1):
            U[j][i] = A[j][i] - sum(U[k][i]*L[j][k] for k in range(j))
        for j in range(i, n):
            L[j][i] = (A[j][i] - sum(U[k][i]*L[j][k] for k in range(i))) / U[i][i]
        print(f"Шаг {i+1}:\nL:")
        print_matrix(L)
        print("U:")
        print_matrix(U)

    return L, U

def solve_lu(L, U, b):
    n = len(L)
    y = [0]*n
    x = [0]*n

    for i in range(n):
        y[i] = b[i] - sum(L[i][j]*y[j] for j in range(i))
        print('y', [float(f"{elem:.3f}") for elem in y])
    for i in range(n-1, -1, -1):
        x[i] = (y[i] - sum(U[i][j]*x[j] for j in range(i+1, n))) / U[i][i]
        print('x', [float(f"{elem:.3f}") for elem in x])

    return x

# Пример использования:
A = [
  [ 1.76,  0.06,  0.20],
  [-0.10,  0.78,  0.44],
  [-0.08, -0.42, -0.50]
]

b = [2.36, 2.78, -2.42]
L, U = lu_decomposition(A)
x = solve_lu(L, U, b)
print(f"Решение системы: {[f'{i:.3f}' for i in x]}")
