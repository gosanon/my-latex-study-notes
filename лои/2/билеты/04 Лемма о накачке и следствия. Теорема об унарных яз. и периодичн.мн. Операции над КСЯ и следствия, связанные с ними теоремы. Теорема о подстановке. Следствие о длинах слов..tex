\documentclass{opaper}

\usepackage{graphicx}
\graphicspath{ {./images/} }

\newcommand{\vd}{\varDelta}
\newcommand{\vt}{\tau}

\begin{document}
  \task{Вопрос 4.}{Лемма о накачке. Следствия о неКСЯ. Теорема об унарных языках и
  периодических множествах. Операции над КСЯ. Теорема о подстановке.
  Следствия об операциях. Пересечение КСЯ. Следствие о длинах слов.
  Теорема о пересечении с регулярным языком.}

  \bigskip
  \m ТЕОРЕМЫ: ЛЕММА О НАКАЧКЕ \s
  
  Назначение: Довольно часто можно показать, что тот или иной язык не является
  контекстно-свободным, установив отсутствие у него данного свойства. Также, с ее помощью
  можно эффективно охарактеризовать КС-языки над алфавитом из одной буквы.

  Формулировка: Для любого КС-языка $L$ существуют натуральные числа $n$ и $m$ такие, что
  любая цепочка $w \in L$ с условием $|w| > n$ представима в виде $w = xuzvy$, где: \fl {
    1)\ uv\neq\eps
    \nl2)\ |uzv| \leqs m
    \nl3)\ x u^k z v^k y \in L \tpp{ для любого натурального } k.
  }

  Доказательство:

  Если язык $L$ является конечным, то теорема очевидно справедлива, поэтому далее мы считаем,
  что $L$ бесконечен.

  Зафиксируем произвольную КС-грамматику $G = (\Sigma, \Gamma, P, S)$, порождающую $L$.
  Для каждой цепочки $w \in L$ можно выбрать дерево вывода в $G$ с минимальным числом вершин.
  Далее рассматриваются только такие минимальные деревья.

  Рассмотрим все минимальные деревья высоты не более $|\Gamma|$ (их конечное число) и в
  качестве $n$ возьмем максимальную длину цепочки, вывод которой представлен таким деревом.
  Кроме того, положим $m = M^{|\Gamma|+1}$, где M – наибольшая длина правой части правила
  в $P$.

  Пусть $w \in L$ и $|w| > n$. Рассмотрим минимальное дерево вывода
  $T$ цепочки $w$.
  
  Его высота больше $|\Gamma|$ по выбору $n$, а значит, в самом
  длинном пути от корня до листа найдутся две вершины, помеченные
  одним и тем же нетерминалом. Пусть этот путь имеет вид $s_0, ..., s_r$,
  а узлы $s_i$ и $s_j$ (где $i < j$) помечены нетерминалом $A$.
  
  Без ограничения общности можно считать, что $r - i \leqs |\Gamma| + 1$, поскольку
  среди любых последовательных $|\Gamma| + 1$ внутренних узлов пути есть
  два одинаково помеченных.

  \begin{center}
    \includegraphics[scale=0.5]{7_1.png}
  \end{center}

  Наша цель – доказать, что разбиение $w = xuzvy$, указанное на
  рис. 3.5, \\удовлетворяет всем условиям теоремы. Для этого введем в
  рассмотрение следующие поддеревья дерева $T$:
  
  - $T_1$, состоящее из узла $s_i$ и всех его потомков,
  \\- $T_2$, состоящее из узла $s_j$ и всех его потомков,
  \\- $T'$, получаемое из $T$ удалением всех потомков узла $s_i$
  \\- $T'_1$, получаемое из $T_1$ удалением всех потомков узла $s_j$.
  
  Дерево $T$ представляет вывод $S\thenn^* w$, \\а его стандартное поддерево $T'$
  – вывод $S \thenn^* xAy$.
  
  Дерево $T_1$ представляет вывод $A \thenn^* uzv$, \\а его стандартное
  поддерево $T'_1$ – вывод $A \thenn^* uAv$.
  
  Наконец, дерево $T_2$ представляет вывод
  $A \thenn^* z$.
  
  Следовательно, $xu^kzv^ky \in L$ для любого k: \fl {
    S
    \nl\thenn^* xAy
    \nl\thenn^* xuAvy
    \nl\thenn^* xu^2Av^2y
    \nl\thenn^* ...
    \nl\thenn^* xu^kAv^ky
    \nl\thenn^* xu^kzv^ky.
  }
  
  Проверим, что $uv \neq\eps$.
  
  Действительно, в противном случае $T'_1$ есть дерево вывода $A \thenn^* A$.
  Но тогда вершины $s_i$ и $s_j$ можно отождествить, получая дерево вывода цепочки $w$,
  содержащее меньше вершин, нежели дерево $T$. Это противоречит выбору $T$.
  
  Осталось показать, что $|uzv| \leqs m$. Высота дерева $T_1$ равна $r - i$
  (если бы в этом дереве существовал путь более длинный, чем путь $s_i, ..., s_r$, то
  путь $s_0, ..., s_r$ не был бы самым длинным в дереве $T$).

  Следовательно, в $T_1$ не более $M^{r-i}$ листьев, а длина цепочки uzv очевидно не
  больше количества листьев. Из условия $r - i \leqs |\Gamma| + 1$ и определения $m$
  получаем требуемое неравенство.
  
  Доказательство завершено.

  \bigskip\bigskip
  \m ОПРЕДЕЛЕНИЯ

  \medskip
  \olist {
    {Периодическое множество $M$}
    /{- [$M\in\N\cup\set{0}$] $M$, если существуют натуральные числа $n_0$ (индекс) и $d$ (период)
    такие, что \fl {
      \forall n\geqs n_0\ \brackets{(n \in M) \thenn (n + d \in M)}
    }},
    %
    {Подстановка}
    /{- \later}
  }

  \m{}ЗАМЕЧАНИЯ, ТЕОРЕМЫ И АЛГОРИТМЫ

  \medskip
  \olistm {
    {Первый пример неКСЯ языка}
    /{Язык $L=\set{a^nb^na^n\sep n\in\N}$ не является контекстно-свободным.
    \mnext ДОКАЗАТЕЛЬСТВО: \later},
    %
    {Второй пример неКСЯ языка}
    /{Язык $L=\set{wcw\sep w\in\set{a,b}^*}$ не является контекстно-свободным.
    \mnext ДОКАЗАТЕЛЬСТВО: \later},
    %
    {Замечание о периодических множествах}
    /{$M$ периодично, если оно есть объединение арифметических операций.
    \mnext ДОКАЗАТЕЛЬСТВО: \later},
    %
    {Теорема об унарных языках и периодических множествах}
    /{Следующие условия эквивалентны для языка $L$ над алфавитом $\set{a}$: \fl {
      1)\ L \tpp{- контекстно-свободный язык};
      \nl 2)\ L \tpp{- регулярный язык};
      \nl 3)\ M = \set{\: |w| \sep w\in L \:} \tpp{- периодическое множество}.
    }
    \mnext ДОКАЗАТЕЛЬСТВО: \later},
    %
    {Теорема 3.3 (о подстановках)}
    /{Пусть $\vt$ – подстановка из произвольного конечного алфавита $\Sigma$ в произвольный
    конечный алфавит $\vd$ такая, что для любой буквы $a\in\Sigma$ язык $\vt(a)$ является
    контекстно-свободным.
    \mnext Тогда если $L\in\Sigma^*$ – КС-язык, то $\vt(L)$ также является КС-языком.
    \mnext ДОКАЗАТЕЛЬСТВО: \later},
    %
    {Следствие (3.1) [из теоремы о подстановках] о замкнутости КСЯ относительно объединения,
    произведения и итерации}
    /{Класс КС-языков замкнут относительно объединения, произведения и итерации.
    \mnext ДОКАЗАТЕЛЬСТВО: \later},
    %
    {Следствие (3.2) [из теоремы о подстановках] о замкнутости КСЯ относительно перехода к
    гомоморфным образам}
    /{Класс КС-языков замкнут относительно перехода к гомоморфным образам.
    \mnext ДОКАЗАТЕЛЬСТВО: \later},
    %
    {Следствие (3.3)}
    /{Если $L$ – КС-язык, то множество $\set{|w| \sep w \in L}$ является периодическим.
    \mnext ДОКАЗАТЕЛЬСТВО: \later},
    %
    {Предложение (3.1) о незамкнутости КСЯ относительно пересечения и дополнения}
    /{Класс КС-языков не замкнут относительно пересечения и дополнения.
    \mnext ДОКАЗАТЕЛЬСТВО: \later},
    %
    {Теорема (3.4) о пересечении КСЯ с регулярным языком}
    /{Пересечение КС-языка с регулярным языком является КС-языком.
    \mnext ДОКАЗАТЕЛЬСТВО: \later}
  }
\end{document}
